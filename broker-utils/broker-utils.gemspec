Gem::Specification.new do |s|
  s.author = 'XWPS L.L.C.'
  s.name = %q(broker-utils)
  s.version = '0.0.1'
  s.date = %q{2020-01-18}
  s.summary = %q(utils aspect of broker interaction)
  s.require_paths = %w(lib)

  s.add_development_dependency 'bundler', '~> 2.0'
  s.add_development_dependency 'rake', '~> 10.0'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'openssl', '~> 2.1.2'
  s.add_development_dependency 'broker-services', '0.0.1'
  s.add_development_dependency 'broker-models', '0.0.1'
end