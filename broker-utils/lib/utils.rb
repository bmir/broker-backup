module Utils
  require 'utils/api_query'
  require 'utils/calculations'
  require 'utils/db_logger'
  require 'utils/properties'
  require 'utils/stock_historical_data'
end

module Security
  require 'security/encryption'
  require 'security/credentials'
  require 'security/key'
end