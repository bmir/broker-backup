Gem::Specification.new do |s|
  s.author = 'XWPS L.L.C.'
  s.name = %q(broker-services)
  s.version = '0.0.1'
  s.date = %q{2020-01-18}
  s.summary = %q(services aspect of broker interaction)
  s.require_paths = %w(lib)

  s.add_development_dependency 'bundler', '~> 2.0'
  s.add_development_dependency 'rake', '~> 10.0'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'broker-models', '0.0.1'
  s.add_development_dependency 'broker-utils', '0.0.1'
  s.add_development_dependency 'json'
  s.add_development_dependency 'oauth'
  s.add_development_dependency 'em-http-request'
  s.add_development_dependency 'alpaca-trade-api'
end