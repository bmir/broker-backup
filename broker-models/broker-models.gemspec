Gem::Specification.new do |s|
  s.author = 'XWPS L.L.C.'
  s.name = %q(broker-models)
  s.version = "0.0.1"
  s.date = %q{2020-01-20}
  s.summary = %q(models of broker interaction)
  s.require_paths = %w(lib)

  s.add_development_dependency 'bundler', '~> 2.0'
  s.add_development_dependency 'rake', '~> 10.0'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'openssl'
  s.add_development_dependency 'broker-services', '~> 0.0.1'
  s.add_development_dependency 'broker-utils', '~> 0.0.1'
end