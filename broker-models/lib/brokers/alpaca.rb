require_relative '../models/rb'

module Brokers
  class Alpaca < Models::Broker
    def initialize(broker)
      super(broker)
    end
  end
end
