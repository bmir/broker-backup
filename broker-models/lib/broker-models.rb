module Brokers
  require 'brokers/ally'
  require 'brokers/alpaca'
end

require 'account'
require 'broker'
require 'broker_manager'
require 'clock'
require 'constants'
require 'decision'
require 'order'
require 'quote'
require 'stock_analysis'
require 'stock_data'
require 'stock_holding'
require 'trader'